package helpers

import "errors"

func PanicIfErr(err error, excludeErr ...error) {
	if err != nil && !ErrContains(err, excludeErr) {
		panic(err)
	}
}

func ErrContains(err error, errs []error) bool {
	for _, v := range errs {
		if errors.Is(err, v) {
			return true
		}
	}
	return false
}
