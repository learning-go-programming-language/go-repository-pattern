package helpers

import (
	"fmt"
	"strconv"
	"strings"
)

func ReplaceSQL(stmt, pattern string, len int) string {
	pattern += ","
	stmt = fmt.Sprintf(stmt, strings.Repeat(pattern, len))
	n := 0
	for strings.IndexByte(stmt, '?') != -1 {
		n++
		param := "$" + strconv.Itoa(n)
		stmt = strings.Replace(stmt, "?", param, 1)
	}
	return strings.TrimSuffix(stmt, ",")
}

func SubstitutePlaceholderSQL(data string, startInt int) (res string) {
	placeholderCount := strings.Count(data, "?")
	res = data
	for i := startInt; i < startInt+placeholderCount; i++ {
		res = strings.Replace(res, "?", "$"+strconv.Itoa(i), 1)
	}
	return res
}
