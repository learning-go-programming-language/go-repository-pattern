package helpers

import (
	"context"
	"database/sql"
)

type CtxTransactionKey struct{}

func GetSqlTx(ctx context.Context) *sql.Tx {
	if txv, ok := ctx.Value(CtxTransactionKey{}).(*sql.Tx); ok {
		return txv
	}

	return nil
}
