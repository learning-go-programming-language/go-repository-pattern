package main

import (
	"context"
	"database/sql"
	"fmt"
	"go-repository-pattern/helpers"
	"go-repository-pattern/models"
	"go-repository-pattern/repositories"
	"go-repository-pattern/repositories/mongodb"
	"go-repository-pattern/repositories/postgres"
	"log"
	"os"

	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	postgreDB   *sql.DB
	mongoClient *mongo.Client
)

func init() {
	conf := SetupConfig()
	postgreDB = SetupPostgresDB(conf.Postgres)

	mongoClient = SetupMongoDB(conf.MongoDB)

}

func main() {
	defer func() {
		if postgreDB != nil {
			postgreDB.Close()
		}
		if mongoClient != nil {
			defer func() {
				if err := mongoClient.Disconnect(context.TODO()); err != nil {
					panic(err)
				}
			}()
		}
	}()

	// run code here
	ctx := context.Background()
	data := models.Transaction{
		Code:         "TRX001",
		CustomerName: "CUSTOMER 1",
		Items: []models.TransactionItem{
			{ProductCode: "PRODUCT001", Qty: 3},
			{ProductCode: "PRODUCT002", Qty: 2},
		},
	}

	/*
		repoImplementation:
			postgres, mongo
		cases:
			run_repository, run_repository_with_transactional, run_repository_with_atomic
	*/
	// run(ctx, data, "run_repository", "postgres")
	// run(ctx, data, "run_repository_with_transactional", "postgres")
	run(ctx, data, "run_repository_with_atomic", "postgres")
}

func run(ctx context.Context, data models.Transaction, cases, repoImplementation string) {
	// postgres repo
	postgresTransactionAndItemRepo := postgres.NewPostgreSQLTransactionAndItemRepository(postgreDB)
	postgresTransactionWithTxRepo := postgres.NewPostgreSQLTransactionWithTxRepository(postgreDB)
	// postgresTransactionRepo := postgres.NewPostgreSQLTransactionRepository(postgreDB) this object can be used inside postgresAggRepo too
	postgresTransactionItemRepo := postgres.NewPostgreSQLTransactionItemRepository(postgreDB)
	postgresAggRepo := postgres.NewPostgresRepository(postgreDB, postgresTransactionWithTxRepo, postgresTransactionItemRepo)
	// mongo repo
	mongoRepo := mongodb.NewMongoTransactionRepository(mongoClient)

	var tr repositories.TransactionRepository
	var trTx repositories.TransactionWithTxRepository
	var tri repositories.TransactionItemRepository
	var r repositories.Repository

	switch repoImplementation {
	case "postgres":
		tr = postgresTransactionAndItemRepo
		trTx = postgresTransactionWithTxRepo
		tri = postgresTransactionItemRepo
		r = postgresAggRepo
	case "mongo":
		tr = mongoRepo
		trTx = nil
		tri = nil
		r = nil
	}

	var err error
	switch cases {
	case "run_repository":
		err = repositories.RunRepository(ctx, &data, tr)
	case "run_repository_with_transactional":
		err = repositories.RunRepositoryWithTransactional(ctx, &data, trTx, tri)
	case "run_repository_with_atomic":
		err = repositories.RunRepositoryWithAtomic(ctx, &data, r)
	}
	if err != nil {
		log.Fatal("error ", err)
	}
}

func SetupConfig() *Config {
	// load default env setup from .env file
	godotenv.Load()

	return &Config{
		Postgres: PostgresConfig{
			Host:     os.Getenv("POSTGRES_HOST"),
			Port:     helpers.StrToInt(os.Getenv("POSTGRES_PORT")),
			Username: os.Getenv("POSTGRES_USERNAME"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
			DB:       os.Getenv("POSTGRES_DB"),
		},
		MongoDB: MongoDBConfig{
			URI: os.Getenv("MONGODB_URI"),
		},
	}
}

func SetupPostgresDB(conf PostgresConfig) *sql.DB {
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		conf.Host,
		conf.Port,
		conf.Username,
		conf.Password,
		conf.DB)

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Panic(err)
	}

	return db
}

func SetupMongoDB(conf MongoDBConfig) *mongo.Client {
	mongoClient, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(conf.URI))
	if err != nil {
		log.Panic(err)
	}

	return mongoClient
}
