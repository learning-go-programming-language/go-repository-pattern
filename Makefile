run:
	go run *.go    
docker-compose-up: 
	docker-compose up -d
docker-compose-down: 
	docker-compose down
repository-mocks:
	mockgen -source=repositories/repository.go -destination=mocks/repository.go -package=mocks
