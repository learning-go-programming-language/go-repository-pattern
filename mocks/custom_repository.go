package mocks

import (
	context "context"
	"log"

	gomock "github.com/golang/mock/gomock"
)

func NewCustomMockTransactionWithTxRepository(ctrl *gomock.Controller) *CustomMockTransactionWithTxRepository {
	mock := &MockTransactionWithTxRepository{ctrl: ctrl}
	mock.recorder = &MockTransactionWithTxRepositoryMockRecorder{mock}
	customMock := &CustomMockTransactionWithTxRepository{
		MockTransactionWithTxRepository: mock,
	}
	return customMock
}

type CustomMockTransactionWithTxRepository struct {
	*MockTransactionWithTxRepository
}

// override "WithTx" function
// using custom mock for cover code inside "WithTx" function
// just run "WithTx" func instead called "WithTx" function
func (mr *CustomMockTransactionWithTxRepository) WithTx(ctx context.Context, fn func(context.Context) error) error {
	log.Println("Inside CustomMockTransactionWithTxRepository", "WithTx")
	return fn(ctx)
}
