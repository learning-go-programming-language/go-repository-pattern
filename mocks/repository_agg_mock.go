package mocks

import (
	context "context"
	repositories "go-repository-pattern/repositories"
)

// repository aggregate mock

type repositoryAggMock struct {
	tr  repositories.TransactionRepository
	tir repositories.TransactionItemRepository
}

func NewRepositoryAggMock(tr repositories.TransactionRepository, tir repositories.TransactionItemRepository) repositories.Repository {
	return &repositoryAggMock{tr: tr, tir: tir}
}

func (r *repositoryAggMock) Atomic(ctx context.Context, steps func(ctx context.Context, r repositories.Repository) error) (err error) {
	err = steps(ctx, r)
	return
}

func (r *repositoryAggMock) GetTransactionRepository() repositories.TransactionRepository {
	return r.tr
}

func (r *repositoryAggMock) GetTransactionItemRepository() repositories.TransactionItemRepository {
	return r.tir
}
