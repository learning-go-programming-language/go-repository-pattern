-- Active: 1671549782426@@127.0.0.1@5433@go_repository_pattern@public
CREATE DATABASE go_repository_pattern;
-- uuid extension
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE transactions (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
	code TEXT NOT NULL CHECK (char_length(code) <= 50),
	customer_name TEXT NOT NULL CHECK (char_length(customer_name) <= 100),
	date TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP --transaction date
);
CREATE TABLE transaction_items (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4 (),
    transaction_id uuid NOT NULL REFERENCES transactions(id),
	product_code TEXT NOT NULL CHECK (char_length(product_code) <= 50),
	qty INTEGER NOT NULL DEFAULT 0 -- quantity
);

-- rollback 
DROP TABLE transaction_items;
DROP TABLE transactions;

-- query data
DELETE FROM transaction_items;
DELETE FROM transactions; 
SELECT * FROM transactions;