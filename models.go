package main

import "time"

type Config struct {
	Postgres PostgresConfig
	MongoDB  MongoDBConfig
}

type PostgresConfig struct {
	Host     string
	Port     int
	Username string
	Password string
	DB       string
}
type MongoDBConfig struct {
	URI string
}

type Transaction struct {
	ID              int
	Code            string
	TransactionDate time.Time
	Status          string
	CreatedAt       time.Time
	Items           []TransactionItem
}

type TransactionItem struct {
	ID            int
	TransactionID int
	ProductCode   string
	CreatedAt     time.Time
}
