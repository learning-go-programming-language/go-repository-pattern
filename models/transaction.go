package models

import (
	"time"

	"github.com/google/uuid"
)

type Transaction struct {
	ID           uuid.UUID
	Code         string
	CustomerName string
	Date         time.Time
	Items        []TransactionItem
}

// separate implementation model for easy maintance
// In case if we use mongo we need to add struct tag (struct field tag) "bson"
type TransactionMongo struct {
	ID           string                 `bson:"_id"` // uuid.UUID is 16 byte so we can store it to string in mongo, https://stackoverflow.com/questions/64723089/how-to-store-a-uuid-in-mongodb-with-golang
	Code         string                 `bson:"code,omitempty"`
	CustomerName string                 `bson:"customer_name,omitempty"`
	Date         time.Time              `bson:"date,omitempty"`
	Items        []TransactionItemMongo `bson:"items,omitempty"`
}

func (t *TransactionMongo) FromModel(model *Transaction) {
	t.ID = model.ID.String()
	t.Code = model.Code
	t.CustomerName = model.CustomerName
	t.Date = model.Date
	for _, item := range model.Items {
		t.Items = append(t.Items, TransactionItemMongo{
			ProductCode: item.ProductCode,
			Qty:         item.Qty,
		})
	}
}

func (t TransactionMongo) ToModel() *Transaction {
	uuidT, _ := uuid.Parse(t.ID)
	model := Transaction{
		ID:           uuidT,
		Code:         t.Code,
		CustomerName: t.CustomerName,
		Date:         t.Date,
	}

	for _, item := range t.Items {
		model.Items = append(model.Items, TransactionItem{
			ProductCode: item.ProductCode,
			Qty:         item.Qty,
		})
	}

	return &model
}

// while postgres doesn't need that
type TransactionPostgres struct {
	ID           uuid.UUID
	Code         string
	CustomerName string
	Date         time.Time
	Items        []TransactionItemPostgres
}

func (t *TransactionPostgres) FromModel(model *Transaction) {
	t.ID = model.ID
	t.Code = model.Code
	t.CustomerName = model.CustomerName
	t.Date = model.Date
	for _, item := range model.Items {
		t.Items = append(t.Items, TransactionItemPostgres{
			ID:            item.ID,
			TransactionID: item.TransactionID,
			ProductCode:   item.ProductCode,
			Qty:           item.Qty,
		})
	}
}

func (t TransactionPostgres) ToModel() *Transaction {
	model := Transaction{
		ID:           t.ID,
		Code:         t.Code,
		CustomerName: t.CustomerName,
		Date:         t.Date,
	}

	for _, item := range t.Items {
		model.Items = append(model.Items, TransactionItem{
			ID:            item.ID,
			TransactionID: item.TransactionID,
			ProductCode:   item.ProductCode,
			Qty:           item.Qty,
		})
	}

	return &model
}

// another example is when you use GORM (ORM library in golang)
// you have to define struct tag 'gorm:"column:column_name"' etc..
