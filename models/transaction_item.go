package models

import (
	"github.com/google/uuid"
)

type TransactionItem struct {
	ID            uuid.UUID
	TransactionID uuid.UUID
	ProductCode   string
	Qty           int
}

// separate implementation model for easy maintance
type TransactionItemMongo struct {
	// mongodb is document based (no sql),
	// so i think we dont need id and transaction id
	//since transaction item will be in one document with the transaction
	ProductCode string `bson:"product_code"`
	Qty         int    `bson:"qty"`
}

type TransactionItemPostgres struct {
	ID            uuid.UUID
	TransactionID uuid.UUID
	ProductCode   string
	Qty           int
}
