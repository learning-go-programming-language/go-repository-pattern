package mongodb

import (
	"context"
	"fmt"
	"go-repository-pattern/constants"
	"go-repository-pattern/models"
	"go-repository-pattern/repositories"
	"log"
	"time"

	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type mongoTransactionRepository struct {
	db *mongo.Client
}

func NewMongoTransactionRepository(db *mongo.Client) repositories.TransactionRepository {
	return &mongoTransactionRepository{
		db: db,
	}
}

func (r *mongoTransactionRepository) FindOneByCode(ctx context.Context, code string) (data *models.Transaction, err error) {
	log.Println(fmt.Sprintf("%T", r), "FindOneByCode")
	res := models.TransactionMongo{}
	filter := bson.D{{Key: "code", Value: code}}
	err = r.db.Database("go_repository_pattern").Collection("transactions").FindOne(ctx, filter).Decode(&res)
	if err == mongo.ErrNoDocuments {
		err = constants.ErrDataNotFound
	}

	data = res.ToModel()
	return
}

func (r *mongoTransactionRepository) Create(ctx context.Context, data *models.Transaction) (id uuid.UUID, err error) {
	log.Println(fmt.Sprintf("%T", r), "Create")
	model := models.TransactionMongo{}
	model.FromModel(data)
	model.ID = uuid.New().String()
	model.Date = time.Now().UTC()
	result, err := r.db.Database("go_repository_pattern").Collection("transactions").InsertOne(ctx, model)
	log.Println(fmt.Sprintf("%T", r), "result", result)
	resID, _ := result.InsertedID.(string)
	id, _ = uuid.Parse(resID)

	return
}
