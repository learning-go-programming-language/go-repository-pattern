package repositories

import (
	"context"
	"go-repository-pattern/constants"
	"go-repository-pattern/models"
	"log"

	"github.com/google/uuid"
)

func RunRepository(ctx context.Context, data *models.Transaction, repo TransactionRepository) error {
	log.Printf("input %+v\n", data)
	exist, err := repo.FindOneByCode(ctx, data.Code)
	if err != nil && err != constants.ErrDataNotFound {
		return err
	}
	if len(exist.Code) > 0 {
		return constants.ErrDataAlreadyExist
	}

	id, err := repo.Create(ctx, data)
	if err != nil {
		return err
	}

	log.Println("transaction id : ", id)
	return nil
}

func RunRepositoryWithTransactional(ctx context.Context, data *models.Transaction, repo TransactionWithTxRepository, repoItem TransactionItemRepository) error {
	log.Printf("input %+v\n", data)
	exist, err := repo.FindOneByCode(ctx, data.Code)
	if err != nil && err != constants.ErrDataNotFound {
		return err
	}
	if exist.ID != uuid.Nil {
		return constants.ErrDataAlreadyExist
	}

	// transactional database done here
	var transactionID uuid.UUID
	err = repo.WithTx(ctx, func(ctx context.Context) error {
		var errTx error

		transactionID, errTx = repo.Create(ctx, data)
		if errTx != nil {
			return errTx
		}

		for i := range data.Items {
			data.Items[i].TransactionID = transactionID
		}

		errTx = repoItem.CreateBulk(ctx, data.Items)
		if errTx != nil {
			return errTx
		}

		return nil
	})
	if err != nil {
		return err
	}

	log.Println("transaction id : ", transactionID)
	return nil
}

func RunRepositoryWithAtomic(ctx context.Context, data *models.Transaction, repo Repository) error {
	log.Printf("input %+v\n", data)
	// transactional databaase done here
	var transactionID uuid.UUID
	err := repo.Atomic(ctx, func(ctx context.Context, r Repository) error {
		var errTx error

		tr := repo.GetTransactionRepository()
		tri := r.GetTransactionItemRepository()

		exist, err := tr.FindOneByCode(ctx, data.Code)
		if err != nil && err != constants.ErrDataNotFound {
			return err
		}
		if exist.ID != uuid.Nil {
			return constants.ErrDataAlreadyExist
		}

		transactionID, errTx = tr.Create(ctx, data)
		if errTx != nil {
			return errTx
		}

		for i := range data.Items {
			data.Items[i].TransactionID = transactionID
		}

		errTx = tri.CreateBulk(ctx, data.Items)
		if errTx != nil {
			return errTx
		}

		return nil
	})
	if err != nil {
		return err
	}

	log.Println("transaction id : ", transactionID)
	return nil
}
