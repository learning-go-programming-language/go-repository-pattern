package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"go-repository-pattern/constants"
	"go-repository-pattern/helpers"
	"go-repository-pattern/models"
	"go-repository-pattern/repositories"
	"log"

	"github.com/google/uuid"
)

// postgresql with tx implementation

// Transaction implementation
// also fit with transaction repository interface
// since the TransactionWithTxRepository only have one different method / function (WithTx)
type postgreSQLTransactionWithTxRepository struct {
	db *sql.DB
}

func NewPostgreSQLTransactionWithTxRepository(db *sql.DB) repositories.TransactionWithTxRepository {
	return &postgreSQLTransactionWithTxRepository{db}
}

func (r *postgreSQLTransactionWithTxRepository) WithTx(ctx context.Context, fn func(context.Context) error) error {
	tx, err := r.db.BeginTx(ctx, &sql.TxOptions{})
	if err != nil {
		return err
	}

	defer func() { // always do rollback, so we dont care to check if err occured to doing rollback
		errRB := tx.Rollback() // do not use err variable , it will override the actual error
		if errRB != nil {
			log.Println("err rollback", errRB)
		}
	}()

	trxCtx := context.WithValue(ctx, helpers.CtxTransactionKey{}, tx)
	err = fn(trxCtx)
	if err != nil {
		return err
	}

	return tx.Commit()
}

func (r *postgreSQLTransactionWithTxRepository) FindOneByCode(ctx context.Context, code string) (data *models.Transaction, err error) {
	log.Println(fmt.Sprintf("%T", r), "FindOneByCode")
	res := models.TransactionPostgres{}
	err = r.db.QueryRowContext(ctx, `SELECT id, code, customer_name, date
		FROM transactions WHERE code = $1 LIMIT 1`, code).
		Scan(&res.ID, &res.Code, &res.CustomerName, &res.Date)
	if err == sql.ErrNoRows {
		err = constants.ErrDataNotFound
	}
	data = res.ToModel()

	return
}

func (r *postgreSQLTransactionWithTxRepository) Create(ctx context.Context, data *models.Transaction) (id uuid.UUID, err error) {
	log.Println(fmt.Sprintf("%T", r), "Create")
	model := models.TransactionPostgres{}
	model.FromModel(data)
	sqlQuery := `INSERT INTO transactions (code, customer_name, date) 
	VALUES( $1, $2, NOW() ) RETURNING id`
	sqlParams := []interface{}{model.Code, model.CustomerName}

	if tx := helpers.GetSqlTx(ctx); tx != nil {
		err = tx.QueryRowContext(ctx, sqlQuery, sqlParams...).Scan(&id)
	} else {
		err = r.db.QueryRowContext(ctx, sqlQuery, sqlParams...).Scan(&id)
	}

	return
}
