package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"go-repository-pattern/helpers"
	"go-repository-pattern/repositories"
)

// implement postgre repository
type postgreRepository struct {
	db  *sql.DB
	tr  repositories.TransactionRepository
	tir repositories.TransactionItemRepository
}

func NewPostgresRepository(db *sql.DB, tr repositories.TransactionRepository, tir repositories.TransactionItemRepository) repositories.Repository {
	return &postgreRepository{db: db, tr: tr, tir: tir}
}

func (r *postgreRepository) Atomic(ctx context.Context, steps func(ctx context.Context, r repositories.Repository) error) (err error) {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		if err != nil {
			if rbErr := tx.Rollback(); rbErr != nil {
				err = fmt.Errorf("tx err: %v, rb err: %v", err, rbErr)
			}
		} else {
			err = tx.Commit()
		}
	}()
	trxCtx := context.WithValue(ctx, helpers.CtxTransactionKey{}, tx)
	err = steps(trxCtx, r)
	return
}

func (r *postgreRepository) GetTransactionRepository() repositories.TransactionRepository {
	return r.tr
}

func (r *postgreRepository) GetTransactionItemRepository() repositories.TransactionItemRepository {
	return r.tir
}
