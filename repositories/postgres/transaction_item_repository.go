package postgres

import (
	"context"
	"database/sql"
	"go-repository-pattern/helpers"
	"go-repository-pattern/models"
	"go-repository-pattern/repositories"
)

// Transaction item implementation
type postgreSQLTransactionItemRepository struct {
	db *sql.DB
}

func NewPostgreSQLTransactionItemRepository(db *sql.DB) repositories.TransactionItemRepository {
	return &postgreSQLTransactionItemRepository{db}
}

func (r *postgreSQLTransactionItemRepository) CreateBulk(ctx context.Context, items []models.TransactionItem) (err error) {

	if len(items) > 0 {
		param := []interface{}{}
		for _, b := range items {
			param = append(param, b.TransactionID, b.ProductCode, b.Qty)
		}
		statement := `INSERT INTO transaction_items (transaction_id, product_code, qty) VALUES %s`
		statement = helpers.ReplaceSQL(statement, "(?, ?, ?)", len(items))
		if tx := helpers.GetSqlTx(ctx); tx != nil {
			_, err = tx.Exec(statement, param...)
		} else {
			_, err = r.db.Exec(statement, param...)
		}
		if err != nil {
			return
		}
	}

	return
}
