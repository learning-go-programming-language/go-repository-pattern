package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"go-repository-pattern/constants"
	"go-repository-pattern/helpers"
	"go-repository-pattern/models"
	"go-repository-pattern/repositories"
	"log"

	"github.com/google/uuid"
)

// postgresql implementation

// postgreSQLTransactionAndItemRepository
// in postgreSQLTransactionAndItemRepository, transactional database done in repository layer for hiding implementation detail about transactional database
type postgreSQLTransactionAndItemRepository struct {
	db *sql.DB
}

func NewPostgreSQLTransactionAndItemRepository(db *sql.DB) repositories.TransactionRepository {
	return &postgreSQLTransactionAndItemRepository{db}
}

func (r *postgreSQLTransactionAndItemRepository) FindOneByCode(ctx context.Context, code string) (data *models.Transaction, err error) {
	log.Println(fmt.Sprintf("%T", r), "FindOneByCode")
	res := models.TransactionPostgres{}
	err = r.db.QueryRowContext(ctx, `SELECT id, code, customer_name, date
		FROM transactions WHERE code = $1 LIMIT 1`, code).
		Scan(&res.ID, &res.Code, &res.CustomerName, &res.Date)
	if err == sql.ErrNoRows {
		err = constants.ErrDataNotFound
	}
	data = res.ToModel()
	return
}

func (r *postgreSQLTransactionAndItemRepository) Create(ctx context.Context, data *models.Transaction) (id uuid.UUID, err error) {
	log.Println(fmt.Sprintf("%T", r), "Create")
	model := models.TransactionPostgres{}
	model.FromModel(data)
	tx, err := r.db.BeginTx(ctx, &sql.TxOptions{}) //for hide implementation transaction detail, transactional database done in repository layer
	if err != nil {
		return
	}
	defer func() {
		errRB := tx.Rollback() // do not use err variable , it will override the actual error
		if errRB != nil {
			log.Println("err rollback", errRB)
		}
	}()
	// insert transaction
	err = tx.QueryRowContext(ctx, `INSERT INTO transactions (code, customer_name, date) 
		VALUES( $1, $2, NOW() ) RETURNING id`, model.Code, model.CustomerName).
		Scan(&id)
	if err != nil {
		return
	}
	// insert transaction items if items > 0
	if len(model.Items) > 0 {
		param := []interface{}{}
		for _, b := range model.Items {
			param = append(param, id, b.ProductCode, b.Qty)
		}
		statement := `INSERT INTO transaction_items (transaction_id, product_code, qty) VALUES %s`
		statement = helpers.ReplaceSQL(statement, "(?, ?, ?)", len(model.Items))
		_, err = tx.Exec(statement, param...)
		if err != nil {
			return
		}
	}

	err = tx.Commit()
	if err != nil {
		return
	}

	return
}

// postgreSQLTransactionRepository just implementing query operation for only table "transactions"
type postgreSQLTransactionRepository struct {
	db *sql.DB
}

func NewPostgreSQLTransactionRepository(db *sql.DB) repositories.TransactionRepository {
	return &postgreSQLTransactionRepository{db}
}

func (r *postgreSQLTransactionRepository) FindOneByCode(ctx context.Context, code string) (data *models.Transaction, err error) {
	log.Println(fmt.Sprintf("%T", r), "FindOneByCode")
	res := models.TransactionPostgres{}
	err = r.db.QueryRowContext(ctx, `SELECT id, code, customer_name, date
		FROM transactions WHERE code = $1 LIMIT 1`, code).
		Scan(&res.ID, &res.Code, &res.CustomerName, &res.Date)
	if err == sql.ErrNoRows {
		err = constants.ErrDataNotFound
	}
	data = res.ToModel()
	return
}

func (r *postgreSQLTransactionRepository) Create(ctx context.Context, data *models.Transaction) (id uuid.UUID, err error) {
	log.Println(fmt.Sprintf("%T", r), "Create")
	model := models.TransactionPostgres{}
	model.FromModel(data)
	sqlQuery := `INSERT INTO transactions (code, customer_name, date) 
	VALUES( $1, $2, NOW() ) RETURNING id`
	sqlParams := []interface{}{model.Code, model.CustomerName}

	if tx := helpers.GetSqlTx(ctx); tx != nil {
		err = tx.QueryRowContext(ctx, sqlQuery, sqlParams...).Scan(&id)
	} else {
		err = r.db.QueryRowContext(ctx, sqlQuery, sqlParams...).Scan(&id)
	}

	return
}
