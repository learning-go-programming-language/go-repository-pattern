package repositories_test

import (
	"context"
	"go-repository-pattern/constants"
	"go-repository-pattern/mocks"
	"go-repository-pattern/models"
	"go-repository-pattern/repositories"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/google/uuid"
)

func TestRunRepository(t *testing.T) {
	ctx := context.Background()
	transactionData := models.Transaction{
		Code:         "MOCK_CODE",
		CustomerName: "MOCK_CUSTOMER",
		Items: []models.TransactionItem{
			{ProductCode: "MOCK_PRODUCT_001", Qty: 3},
			{ProductCode: "MOCK_PRODUCT_002", Qty: 2},
			{ProductCode: "MOCK_PRODUCT_003", Qty: 4},
		},
	}

	type args struct {
		ctx      context.Context
		data     *models.Transaction
		mockRepo func(mock *mocks.MockTransactionRepository)
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "case success",
			args: args{
				ctx:  ctx,
				data: &transactionData,
				mockRepo: func(mock *mocks.MockTransactionRepository) {
					mock.EXPECT().FindOneByCode(ctx, transactionData.Code).Return(&models.Transaction{}, nil).Times(1)
					mock.EXPECT().Create(ctx, &transactionData).Return(uuid.New(), nil).Times(1)
				},
			},
			wantErr: nil,
		},
		{
			name: "case data already exist",
			args: args{
				ctx:  ctx,
				data: &transactionData,
				mockRepo: func(mock *mocks.MockTransactionRepository) {
					newID := uuid.New()
					mock.EXPECT().FindOneByCode(ctx, transactionData.Code).Return(&models.Transaction{ID: newID, Code: "CODE001"}, nil).Times(1)
				},
			},
			wantErr: constants.ErrDataAlreadyExist,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			repo := mocks.NewMockTransactionRepository(mockCtrl)
			tt.args.mockRepo(repo)
			err := repositories.RunRepository(tt.args.ctx, tt.args.data, repo)
			if err != tt.wantErr {
				t.Errorf("RunRepository() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestRunRepositoryWithTransactional(t *testing.T) {
	ctx := context.Background()
	transactionData := models.Transaction{
		Code:         "MOCK_CODE",
		CustomerName: "MOCK_CUSTOMER",
		Items: []models.TransactionItem{
			{ProductCode: "MOCK_PRODUCT_001", Qty: 3},
			{ProductCode: "MOCK_PRODUCT_002", Qty: 2},
			{ProductCode: "MOCK_PRODUCT_003", Qty: 4},
		},
	}
	transactionID := uuid.New()
	for i := range transactionData.Items {
		transactionData.Items[i].TransactionID = transactionID
	}

	type args struct {
		ctx          context.Context
		data         *models.Transaction
		mockRepo     func(mock *mocks.MockTransactionWithTxRepository)
		mockRepoItem func(mock *mocks.MockTransactionItemRepository)
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "success",
			args: args{
				ctx:  ctx,
				data: &transactionData,
				mockRepo: func(mock *mocks.MockTransactionWithTxRepository) {
					mock.EXPECT().FindOneByCode(ctx, transactionData.Code).Return(&models.Transaction{}, nil).Times(1)

					mock.EXPECT().WithTx(gomock.Any(), gomock.Any()).Return(nil).Times(1)
				},
				mockRepoItem: func(mock *mocks.MockTransactionItemRepository) {},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			repo := mocks.NewMockTransactionWithTxRepository(mockCtrl)
			repoItem := mocks.NewMockTransactionItemRepository(mockCtrl)
			tt.args.mockRepo(repo)
			tt.args.mockRepoItem(repoItem)
			err := repositories.RunRepositoryWithTransactional(tt.args.ctx, tt.args.data, repo, repoItem)
			if err != tt.wantErr {
				t.Errorf("RunRepositoryWithTransactional() error = %v, wantErr %v", err, tt.wantErr)
			}

		})
	}
}

// using custom mock for cover code inside "WithTx" function
func TestRunRepositoryWithTransactionalWithCutomMock(t *testing.T) {
	ctx := context.Background()
	transactionData := models.Transaction{
		Code:         "MOCK_CODE",
		CustomerName: "MOCK_CUSTOMER",
		Items: []models.TransactionItem{
			{ProductCode: "MOCK_PRODUCT_001", Qty: 3},
			{ProductCode: "MOCK_PRODUCT_002", Qty: 2},
			{ProductCode: "MOCK_PRODUCT_003", Qty: 4},
		},
	}
	transactionID := uuid.New()
	for i := range transactionData.Items {
		transactionData.Items[i].TransactionID = transactionID
	}

	type args struct {
		ctx          context.Context
		data         *models.Transaction
		mockRepo     func(mock *mocks.CustomMockTransactionWithTxRepository)
		mockRepoItem func(mock *mocks.MockTransactionItemRepository)
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "success",
			args: args{
				ctx:  ctx,
				data: &transactionData,
				mockRepo: func(mock *mocks.CustomMockTransactionWithTxRepository) {
					mock.EXPECT().FindOneByCode(ctx, transactionData.Code).Return(&models.Transaction{}, nil).Times(1)
					mock.EXPECT().Create(ctx, &transactionData).Return(transactionID, nil).Times(1)
				},
				mockRepoItem: func(mock *mocks.MockTransactionItemRepository) {
					mock.EXPECT().CreateBulk(ctx, transactionData.Items).Return(nil).Times(1)
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			repo := mocks.NewCustomMockTransactionWithTxRepository(mockCtrl)
			repoItem := mocks.NewMockTransactionItemRepository(mockCtrl)
			tt.args.mockRepo(repo)
			tt.args.mockRepoItem(repoItem)
			err := repositories.RunRepositoryWithTransactional(tt.args.ctx, tt.args.data, repo, repoItem)
			if err != tt.wantErr {
				t.Errorf("RunRepositoryWithTransactional() error = %v, wantErr %v", err, tt.wantErr)
			}

		})
	}
}

func TestRunRepositoryWithAtomic(t *testing.T) {
	ctx := context.Background()
	transactionData := models.Transaction{
		Code:         "MOCK_CODE",
		CustomerName: "MOCK_CUSTOMER",
		Items: []models.TransactionItem{
			{ProductCode: "MOCK_PRODUCT_001", Qty: 3},
			{ProductCode: "MOCK_PRODUCT_002", Qty: 2},
			{ProductCode: "MOCK_PRODUCT_003", Qty: 4},
		},
	}
	transactionID := uuid.New()
	for i := range transactionData.Items {
		transactionData.Items[i].TransactionID = transactionID
	}

	type args struct {
		ctx          context.Context
		data         *models.Transaction
		mockRepo     func(mock *mocks.MockTransactionWithTxRepository)
		mockRepoItem func(mock *mocks.MockTransactionItemRepository)
	}
	tests := []struct {
		name    string
		args    args
		wantErr error
	}{
		{
			name: "success",
			args: args{
				ctx:  ctx,
				data: &transactionData,
				mockRepo: func(mock *mocks.MockTransactionWithTxRepository) {
					mock.EXPECT().FindOneByCode(ctx, transactionData.Code).Return(&models.Transaction{}, nil).Times(1)
					mock.EXPECT().Create(ctx, &transactionData).Return(transactionID, nil).Times(1)
				},
				mockRepoItem: func(mock *mocks.MockTransactionItemRepository) {
					mock.EXPECT().CreateBulk(ctx, transactionData.Items).Return(nil).Times(1)
				},
			},
		},
		{
			name: "error data already exist",
			args: args{
				ctx:  ctx,
				data: &transactionData,
				mockRepo: func(mock *mocks.MockTransactionWithTxRepository) {
					data := transactionData
					data.ID = transactionID
					mock.EXPECT().FindOneByCode(ctx, transactionData.Code).Return(&data, nil).Times(1)
				},
				mockRepoItem: func(mock *mocks.MockTransactionItemRepository) {},
			},
			wantErr: constants.ErrDataAlreadyExist,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()
			repo := mocks.NewMockTransactionWithTxRepository(mockCtrl)
			repoItem := mocks.NewMockTransactionItemRepository(mockCtrl)
			tt.args.mockRepo(repo)
			tt.args.mockRepoItem(repoItem)
			repoAggregate := mocks.NewRepositoryAggMock(repo, repoItem)
			err := repositories.RunRepositoryWithAtomic(tt.args.ctx, tt.args.data, repoAggregate)
			if err != tt.wantErr {
				t.Errorf("RunRepositoryWithTransactional() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
