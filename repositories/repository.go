package repositories

import (
	"context"
	"go-repository-pattern/models"

	"github.com/google/uuid"
)

type TransactionRepository interface {
	FindOneByCode(ctx context.Context, code string) (data *models.Transaction, err error)
	Create(ctx context.Context, data *models.Transaction) (id uuid.UUID, err error)
}

type TransactionItemRepository interface {
	CreateBulk(ctx context.Context, items []models.TransactionItem) (err error)
}

// implement transactional database between repositories with "InTransaction" function
// main idea is pass sql.Tx struct to context,
// and inside every write function like create,update,delete need to check is there sql.Tx struct in context, if yes use sql.Tx struct
type TransactionWithTxRepository interface { //withTx means with transactions
	WithTx(ctx context.Context, fn func(context.Context) error) error
	FindOneByCode(ctx context.Context, code string) (data *models.Transaction, err error)
	Create(ctx context.Context, data *models.Transaction) (id uuid.UUID, err error)
}

// aggregate all repositories
type Repository interface {
	// transactional database done inside Atomic function
	Atomic(ctx context.Context, steps func(ctx context.Context, r Repository) error) error

	GetTransactionRepository() TransactionRepository
	GetTransactionItemRepository() TransactionItemRepository
}
