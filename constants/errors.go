package constants

import "errors"

var (
	ErrDataNotFound     = errors.New("data_not_found")
	ErrDataAlreadyExist = errors.New("data_already_exist")
)
