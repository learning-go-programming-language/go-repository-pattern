# go-repository-pattern
## Description
Implement repository pattern in go programming language
## Explanation
* interface and implementations put inside repositories folder
* there is 2 (two) example ways for implement transactional database in repository pattern
    * done inside repository layer
        * for this particular choice the downside is we could end up pushing too much business logic to the persistence layer
    * involve some repositories 
        * inject *sql.Tx inside context

## Summary
for transactional database between repository i would like to use aggregate the repository and run the repository function inside "Atomic" function like the example inside the project since the aggregate / centralized repository can acts like orchestrator for each repository